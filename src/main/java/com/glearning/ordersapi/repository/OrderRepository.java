package com.glearning.ordersapi.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.glearning.ordersapi.model.Order;

@Repository
public class OrderRepository {
	private List<Order> orders= new ArrayList<>();
	
	public Order saveOrder(Order order) {
		orders.add(order);
		return order;
	}
	
	public List<Order> fetchOrders(){
		return orders;
	}
	
	public Optional<Order> findOrderById(long orderId){
		return orders.stream().filter(order -> order.getId() == orderId)
				.findAny();
	}
	
	public void deleteOrderById(long orderId) {
		orders.removeIf(order -> order.getId() == orderId);
	}
	
	public Order updateExistingOrder(long orderId, Order updatedOrder) {
		Optional<Order> optionalOrder = orders
											.stream()
											.filter(order -> order.getId() == orderId)
											.findAny();
		if(optionalOrder.isPresent()) {
			Order orderFetchedFromList = optionalOrder.get();
			orderFetchedFromList.setName(updatedOrder.getName());
			orderFetchedFromList.setDate(updatedOrder.getDate());
			orderFetchedFromList.setId(updatedOrder.getId());
			orderFetchedFromList.setPrice(updatedOrder.getPrice());
			return orderFetchedFromList;
		}
		return null;
	}
}
