package com.glearning.ordersapi.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.glearning.ordersapi.model.Order;
import com.glearning.ordersapi.service.OrderService;

@RestController
@RequestMapping("/api/orders")
public class OrderRestController {
	
	private OrderService orderService;
	
	public OrderRestController(OrderService orderService) {
		this.orderService = orderService;
	}
	
	@GetMapping
	public List<Order> fetchAllOrder(){
		return this.orderService.fetchAllOrders();
	}
	
	@GetMapping("/{id}")
	public Order fetchOrderById(@PathVariable long id){
		return this.orderService.findOrderById(id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable long id){
		this.orderService.deleteOrderById(id);
	}
	
	@PutMapping("/{id}")
	public Order updateOrder(@PathVariable long id, @RequestBody Order order) {
		return this.orderService.updateExistingOrder(id, order);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order saveOrder(@RequestBody Order order){
		return this.orderService.saveOrder(order);
	}
}
