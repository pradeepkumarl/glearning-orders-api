package com.glearning.ordersapi.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.glearning.ordersapi.model.Order;
import com.glearning.ordersapi.repository.OrderRepository;



//do the dependency injection of order repository via Spring IOC
@Service
public class OrderService {
	
	//has-a relationship
	private final OrderRepository orderRepository;
	
	public OrderService(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}
	
	public Order saveOrder(Order order) {
		return this.orderRepository.saveOrder(order);
	}
	
	public List<Order> fetchAllOrders(){
		return this.orderRepository.fetchOrders();
	}
	
	public Order findOrderById(long orderId) {
		return this.orderRepository
					.findOrderById(orderId)
					.orElseThrow( () -> new IllegalArgumentException("invalid order id passed "));
	}
	
	public void deleteOrderById(long orderId) {
		System.out.println("Will delete the order matching orderid "+ orderId);
		this.orderRepository.deleteOrderById(orderId);
	}
	
	public Order updateExistingOrder(long orderId, Order updatedOrder) {
		return this.orderRepository.updateExistingOrder(orderId, updatedOrder);
	}

}

