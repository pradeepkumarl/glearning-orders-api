package com.glearning.ordersapi.model;

import java.time.LocalDate;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Order {
	
	private long id;
	private String name;
	private double price;
	private LocalDate date;

}
